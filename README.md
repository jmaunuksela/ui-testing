# Color Picker Selenium tests
Project to test a color picker webb app which transforms between HEX and RGB values using selenium.

The test suite includes 5 tests:
- 'Can findall rgb sliders'
- 'Can find all rgb inputs'
- 'Can get hex value from "value-hex" input field'
- 'Can get and set "value-rgb" input field'
- 'Can move sliders'

To run the tests, write the command 'npm run test'.